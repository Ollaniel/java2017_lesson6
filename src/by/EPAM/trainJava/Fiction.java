package by.EPAM.trainJava;

public class Fiction extends Composition{
	public enum fictType { Science, Fantasy}
	private fictType fType;
	
	Fiction funficLink;
	
	
	public Fiction() {
		super();
		fType=fictType.Science;
		funficLink=null;
	}
	public Fiction(String n, String t, String a, int y, fictType ft, Fiction f) {
		super(n, t, a, y);
		fType=ft;
		funficLink=f;
	}
	
	@Override
	public String toString() {
		return super.toString()+ String.format("%25s%25s\n","Fiction", 
				(funficLink==null)?"":"Фанфик по "+funficLink.getName());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		//result = prime * result + ((fType == null) ? 0 : fType.hashCode());
		//result = prime * result + ((funficLink == null) ? 0 : funficLink.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fiction other = (Fiction) obj;
		if (fType != other.fType)
			return false;
		if (funficLink == null) {
			if (other.funficLink != null)
				return false;
		} else if (!funficLink.equals(other.funficLink))
			return false;
		return true;
	}
	public fictType getfType() {
		return fType;
	}
	public void setfType(fictType fType) {
		this.fType = fType;
	}
	public Fiction getFunfikLink() {
		return funficLink;
	}
	public void setFunfikLink(Fiction funfikLink) {
		this.funficLink = funfikLink;
	}

	
}
