package by.EPAM.trainJava;

import java.util.HashSet;

public class Publicism extends Composition {
	String originalPublishing;
	private HashSet<String> politicalView;
	
	@Override
	public String toString() {
		return super.toString()+ String.format("%25s\n","Publicism");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		//result = prime * result + ((originalPublishing == null) ? 0 : originalPublishing.hashCode());
		//result = prime * result + ((politicalView == null) ? 0 : politicalView.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Publicism other = (Publicism) obj;
		if (originalPublishing == null) {
			if (other.originalPublishing != null)
				return false;
		} else if (!originalPublishing.equals(other.originalPublishing))
			return false;
		if (politicalView == null) {
			if (other.politicalView != null)
				return false;
		} else if (!politicalView.equals(other.politicalView))
			return false;
		return true;
	}

	public String getOriginalPublishing() {
		return originalPublishing;
	}

	public void setOriginalPublishing(String originalPublishing) {
		this.originalPublishing = originalPublishing;
	}
	
	public void addpoliticalView(String a) {
		politicalView.add(a) ;
	}
	public void clearpoliticalView() {
		politicalView.clear();
	}
	
}
