package by.EPAM.trainJava;
import java.util.HashSet;

/***
 * 
 * @author Sergii_Kotov
 * базовый тип произведения
 */
public abstract class Composition {
	/***
	 * название
	 */
	private String name;
	/***
	 * как автор определил жанр 
	 * Не факт что это определение как-то классифицируется
	 */
	private String type;
	/***
	 * автор произведения
	 */
	private String author;
	/***
	 * год написания
	 */
	private int year;
	/***
	 * текст произведения
	 */
	private String text;
	
	/***
	 * название цикла. если произведение в него входит
	 */
	private String cycle;
	
	/***
	 * номер в цикле
	 */
	private int numInCycle;

	/***
	 * список связанных произведений. тех что повлияли, вдохновили 
	 */
	private HashSet<Composition> inspiredBy;;
	
	Composition (){
		this("","","",1980);
	}
			
	/***
	 * основной конструктор 
	 * @param n название
	 * @param t тип по автору
	 * @param a автор
	 * @param y год создания
	 */
	Composition (String n, String t, String a, int y){
		name=n;
		type=t;
		author=a;
		year=y;
		text=null;
		numInCycle=0;
		cycle=null;
		inspiredBy = new HashSet<Composition>();
	}

	/***
	 * хеш только по автору и названию без регистров
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.toLowerCase().hashCode());
		result = prime * result + ((name == null) ? 0 : name.toLowerCase().hashCode());
		return result;
		/*
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((cycle == null) ? 0 : cycle.hashCode());
		result = prime * result + ((inspiredBy == null) ? 0 : inspiredBy.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + numInCycle;
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + year;
		return result;
		*/
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Composition other = (Composition) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (cycle == null) {
			if (other.cycle != null)
				return false;
		} else if (!cycle.equals(other.cycle))
			return false;
		if (inspiredBy == null) {
			if (other.inspiredBy != null)
				return false;
		} else if (!inspiredBy.equals(other.inspiredBy))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (numInCycle != other.numInCycle)
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (year != other.year)
			return false;
		return true;
	}

	/***
	 * для удобной распечатки большого кол-ва книг
	 */
	@Override
	public String toString() {
		return String.format("%25s%25s%25s%25s%25s%25s",name,type,author,year,cycle,numInCycle);
	}

	//	----------------------------------
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	public int getNumInCycle() {
		return numInCycle;
	}

	public void setNumInCycle(int numInCycle) {
		this.numInCycle = numInCycle;
	}

	public void addInspiredBy(Composition inspiredBy) {
		this.inspiredBy.add(inspiredBy);
	}
	
	public void clearInspiredBy() {
		this.inspiredBy.clear();
	}
}
