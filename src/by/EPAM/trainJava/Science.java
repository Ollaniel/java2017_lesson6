package by.EPAM.trainJava;

import java.util.HashSet;

public class Science extends Composition {
	private HashSet<String> coAuthors;
	private boolean isPublishedAndVerified;
	
	
	@Override
	public String toString() {
		return super.toString()+ String.format("%25s","Science");
	}
	
	public Science() {
		super();
		coAuthors=null;
		isPublishedAndVerified=false;
	}
	
	// не стоит так называть поля, даже локальные
	public Science(String n, String t, String a, int y, HashSet<String> ca, boolean f) {
		super(n, t, a, y);
		coAuthors=ca;
		isPublishedAndVerified=f;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coAuthors == null) ? 0 : coAuthors.hashCode());
		result = prime * result + (isPublishedAndVerified ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Science other = (Science) obj;
		if (coAuthors == null) {
			if (other.coAuthors != null)
				return false;
		} else if (!coAuthors.equals(other.coAuthors))
			return false;
		if (isPublishedAndVerified != other.isPublishedAndVerified)
			return false;
		return true;
	}
	
	public void addCoAuthors(String a) {
		coAuthors.add(a) ;
	}
	public void clearCoAuthors() {
		coAuthors.clear();
	}
	public boolean isPublishedAndVerified() {
		return isPublishedAndVerified;
	}
	public void setPublishedAndVerified(boolean isPublishedAndVerified) {
		this.isPublishedAndVerified = isPublishedAndVerified;
	}
	
	
}
